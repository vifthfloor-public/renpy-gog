**Renpy-GOG Achievement Library**

This is a small ctypes library and a python helper class to have simple achievement flags synced to GOG in your Renpy game.

To use this library:
 1. Download the GOG Galaxy SDK for your target platform (Windows 32bit/64bit and/or OSX) from the GOG GALAXY Developer Portal: https://devportal.gog.com/galaxy/components/sdk.
 2. Copy the downloaded library file from the "Libraries" directory to the respective "renpy-gog/lib/{platform}" directories.
 3. Copy the "lib" directory in the "renpy-gog" directory to the base directory (root folder).
 4. Copy the "gog_helper.rpy" and "secret_file.rpy" anywhere within the game directory ("game" folder).

See "example.rpy" for usage example.

To build the library on your own for Windows with Visual Studio, download the GOG Galaxy SDK for Windows 32bit and 64bit from the GOG GALAXY Developer Portal: https://devportal.gog.com/galaxy/components/sdk. Extract the files and copy them into "Source/galaxy-sdk/Windows_MSVC19_32bit" and "Source/galaxy-sdk/Windows_MSVC19_64bit" directory respectively.
