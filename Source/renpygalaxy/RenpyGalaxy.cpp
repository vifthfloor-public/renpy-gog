// RenpyGalaxy.cpp

#include "RenpyGalaxy.h"

extern "C" bool InitGalaxy(const char* clientID, const char* clientSecret) {

	// see:
	// _renpysteam.init()
	// SteamBackend.__init__()
	// GalaxyDemo::InitGalaxy()
	// GameplayData::Init()

	try {
		galaxy::api::Init({ clientID, clientSecret });

		listeners.emplace_back(std::make_unique<AuthListener>());
		listeners.emplace_back(std::make_unique<UserStatsAndAchievementsRetrieveListener>());
		listeners.emplace_back(std::make_unique<AchievementChangeListener>());
		listeners.emplace_back(std::make_unique<StatsAndAchievementsStoreListener>());

		return true;
	}
	catch (const galaxy::api::IError& er) {
		//galaxy::api::Logger()->Warning("Failed to Init: %s", er.GetMsg());
		return false;
	}
}

extern "C" bool SignInGalaxy() {
	try {
		galaxy::api::User()->SignInGalaxy();
		return true;
	}
	catch (const galaxy::api::IError& er) {
		//galaxy::api::Logger()->Warning("Failed to Sign In: %s", er.GetMsg());
		return false;
	}
}

extern "C" bool CheckSignedIn() {
	try {
		bool signedIn = galaxy::api::User()->SignedIn();
		return true;
	}
	catch (const galaxy::api::IError& er) {
		//galaxy::api::Logger()->Warning("Failed to CheckSignedIn: %s", er.GetMsg());
		return false;
	}
}

extern "C" bool SignedIn() {
	try {
		bool signedIn = galaxy::api::User()->SignedIn();
		return signedIn;
	}
	catch (const galaxy::api::IError& er) {
		//galaxy::api::Logger()->Warning("Failed to SignedIn: %s", er.GetMsg());
		return false;
	}
}

extern "C" bool RequestUserStatsAndAchievements() {
	try {
		galaxy::api::Stats()->RequestUserStatsAndAchievements();
		return true;
	}
	catch (const galaxy::api::IError& er) {
		//galaxy::api::Logger()->Error("Failed to retrieve user stats and achievements: %s", er.GetMsg());
		return false;
	}
}

extern "C" bool SetAchievement(const char* achievement_name) {

	// see:
	// SteamBackend.grant()
	// _renpysteam.grant_achievement(name)
	// _renpysteam.store_stats()

	try {
		galaxy::api::Stats()->SetAchievement(achievement_name);
		return true;
	}
	catch (const galaxy::api::IError& er) {
		//galaxy::api::Logger()->Error("Failed to set %s achievement: %s", achievement_name, er.GetMsg());
		return false;
	}
}

extern "C" bool StoreStatsAndAchievements() {

	try {
		galaxy::api::Stats()->StoreStatsAndAchievements();
		return true;
	}
	catch (const galaxy::api::IError& er) {
		//galaxy::api::Logger()->Error("Failed to store achievements: %s", er.GetMsg());
		return false;
	}
}

extern "C" bool CheckGetAchievement(const char* achievement_name) {

	bool unlocked = false;
	uint32_t unlockedTime = 0;

	try {
		galaxy::api::Stats()->GetAchievement(achievement_name, unlocked, unlockedTime);
		return true;
	}
	catch (const galaxy::api::IError& er) {
		//galaxy::api::Logger()->Error("Check: Failed to get %s achievement: %s", achievement_name, er.GetMsg());
		return false;
	}
}

extern "C" bool GetAchievement(const char* achievement_name) {

	bool unlocked = false;
	uint32_t unlockedTime = 0;

	try {
		galaxy::api::Stats()->GetAchievement(achievement_name, unlocked, unlockedTime);
		return unlocked;
	}
	catch (const galaxy::api::IError& er) {
		//galaxy::api::Logger()->Error("Failed to get %s achievement: %s", achievement_name, er.GetMsg());
		return false;
	}
}

extern "C" bool ProcessData() {
	try {
		galaxy::api::ProcessData();
		return true;
	}
	catch (const galaxy::api::IError& er) {
		//galaxy::api::Logger()->Error("Failed to process data. %s", achievement_name, er.GetMsg());
		return false;
	}
}

AuthListener::AuthListener()
{
}

void AuthListener::OnAuthSuccess()
{
}

void AuthListener::OnAuthFailure(FailureReason reason)
{
}

void AuthListener::OnAuthLost()
{
}

UserStatsAndAchievementsRetrieveListener::UserStatsAndAchievementsRetrieveListener()
{
}

void UserStatsAndAchievementsRetrieveListener::OnUserStatsAndAchievementsRetrieveSuccess(galaxy::api::GalaxyID userID)
{
}

void UserStatsAndAchievementsRetrieveListener::OnUserStatsAndAchievementsRetrieveFailure(galaxy::api::GalaxyID userID, FailureReason failureReason)
{
}

AchievementChangeListener::AchievementChangeListener()
{
}

void AchievementChangeListener::OnAchievementUnlocked(const char* name)
{
}

StatsAndAchievementsStoreListener::StatsAndAchievementsStoreListener()
{
}

void StatsAndAchievementsStoreListener::OnUserStatsAndAchievementsStoreSuccess()
{
}

void StatsAndAchievementsStoreListener::OnUserStatsAndAchievementsStoreFailure(FailureReason failureReason)
{
}
