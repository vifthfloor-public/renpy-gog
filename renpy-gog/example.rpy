## How to initialize GOG Helper
init python:

    # Set these variables. See secret_file.rpy.
    global gog_enabled
    global gog_clientID
    global gog_clientSecret

    # Init
    renpygalaxy = GOGHelper(gog_enabled, gog_clientID, gog_clientSecret)





## How to use example
init python:
 
    if (renpygalaxy):
        try:
            renpygalaxy.grant("ACHIEVEMENT_ID")
            renpygalaxy.sync()
        except Exception, e:
            print(e)
