init -1 python:
    import ctypes
    
    class GOGHelper(object):

        def __init__(self, enabled, clientID, clientSecret):
            self.enabled = enabled
            self.clientID = clientID
            self.clientSecret = clientSecret

            print("GOG enabled = " + str(self.enabled))

            if not renpy.windows and not renpy.macintosh:
                self.enabled = False
                print("GOG disabled on platforms other than Windows and MacOS")

            self.MAXIMUM_FRAMERATE = 15
            self.ach_requested_cache = False
            self.requestAchMaxAttempts = 300  # 20s

            self.galaxy_path = None
            self.galaxy = None
            self.renpygalaxy_path = None
            self.renpygalaxy = None

            # Galaxy API Load
            self.load_dlls()
            self.init_galaxy()
            self.register_processdata()

        def load_dlls(self):
            if not self.enabled:
                return

            if renpy.windows:
                if renpy.bits == 64:
                    self.galaxy_path = "/lib/windows-x86_64/Galaxy64.dll"
                    self.renpygalaxy_path = "/lib/windows-x86_64/RenpyGalaxy64.dll"
                else:
                    self.galaxy_path = "/lib/windows-i686/Galaxy.dll"
                    self.renpygalaxy_path = "/lib/windows-i686/RenpyGalaxy.dll"
            elif renpy.macintosh:
                self.galaxy_path = "/lib/mac-x86_64/libGalaxy64.dylib"
                self.renpygalaxy_path = "/lib/mac-x86_64/librenpygalaxy.dylib"

            if self.galaxy_path and self.renpygalaxy_path:
                try:
                    if self.galaxy is None:
                        self.galaxy = ctypes.CDLL(config.basedir.replace('\\', '/') + self.galaxy_path)
                    if self.renpygalaxy is None:
                        self.renpygalaxy = ctypes.CDLL(config.basedir.replace('\\', '/') + self.renpygalaxy_path)
                except Exception, e:
                    print(e)

            if not self.galaxy:
                raise Exception("Galaxy load failed: " + self.galaxy_path)
            if not self.renpygalaxy:
                raise Exception("Galaxy renpy helper load failed: " + self.renpygalaxy_path)

            # Galaxy API Init
            if self.galaxy and self.renpygalaxy:

                self.renpygalaxy.InitGalaxy.restype = ctypes.c_bool
                self.renpygalaxy.SignInGalaxy.restype = ctypes.c_bool
                self.renpygalaxy.CheckSignedIn.restype = ctypes.c_bool
                self.renpygalaxy.SignedIn.restype = ctypes.c_bool
                self.renpygalaxy.RequestUserStatsAndAchievements.restype = ctypes.c_bool
                self.renpygalaxy.SetAchievement.restype = ctypes.c_bool
                self.renpygalaxy.StoreStatsAndAchievements.restype = ctypes.c_bool
                self.renpygalaxy.CheckGetAchievement.restype = ctypes.c_bool
                self.renpygalaxy.GetAchievement.restype = ctypes.c_bool
                self.renpygalaxy.ProcessData.restype = ctypes.c_bool

                self.renpygalaxy.InitGalaxy.argtypes = [ctypes.c_char_p, ctypes.c_char_p]
                self.renpygalaxy.SignInGalaxy.argtypes = []
                self.renpygalaxy.CheckSignedIn.argtypes = []
                self.renpygalaxy.SignedIn.argtypes = []
                self.renpygalaxy.RequestUserStatsAndAchievements.argtypes = []
                self.renpygalaxy.SetAchievement.argtypes = [ctypes.c_char_p]
                self.renpygalaxy.StoreStatsAndAchievements.argtypes = []
                self.renpygalaxy.CheckGetAchievement.argtypes = [ctypes.c_char_p]
                self.renpygalaxy.GetAchievement.argtypes = [ctypes.c_char_p]
                self.renpygalaxy.ProcessData.argtypes = []

        def init_galaxy(self):
            if not self.enabled or not self.galaxy or not self.renpygalaxy:
                return

            try:
                renpy.maximum_framerate(self.MAXIMUM_FRAMERATE)
                init_galaxy = self.renpygalaxy.InitGalaxy(self.clientID, self.clientSecret)
                if init_galaxy:
                    pass
                else:
                    print("galaxy init failed")
            except Exception, e:
                print("galaxy init_galaxy failed: " + str(e))

        def register_processdata(self):
            if not self.enabled or not self.galaxy or not self.renpygalaxy:
                return

            renpy.config.periodic_callbacks.append(self.processdata)

        def processdata(self):
            if not self.enabled or not self.galaxy or not self.renpygalaxy:
                return

            self.renpygalaxy.ProcessData()
            self.request_achievements_once()

        def sign_in(self):
            if not self.enabled or not self.galaxy or not self.renpygalaxy:
                return

            try:
                signin = self.renpygalaxy.SignInGalaxy()
                if signin:
                    print("galaxy SignedIn tried")
                else:
                    print("galaxy SignedIn failed")
            except Exception, e:
                print("galaxy sign_in failed: " + str(e))

        def request_achievements_once(self):
            if not self.enabled or not self.galaxy or not self.renpygalaxy:
                return

            if self.ach_requested_cache or self.requestAchMaxAttempts < 1:
                self.requestAchMaxAttempts = 0
                return

            if not self.signed_in():
                self.sign_in()
                self.requestAchMaxAttempts -= 1

            if not self.signed_in():
                print("galaxy RequestUserStatsAndAchievements failed. Not signed in. Attempts left: " + str(self.requestAchMaxAttempts))
                return

            try:
                request = self.renpygalaxy.RequestUserStatsAndAchievements()
                if request:
                    print("galaxy RequestUserStatsAndAchievements tried")
                    self.ach_requested_cache = True
                else:
                    print("galaxy RequestUserStatsAndAchievements failed")
            except Exception, e:
                print("galaxy RequestUserStatsAndAchievements failed: " + str(e))

        def signed_in(self):
            if not self.enabled or not self.galaxy or not self.renpygalaxy:
                return

            try:
                can_check_signedin = self.renpygalaxy.CheckSignedIn()
                signedin = self.renpygalaxy.SignedIn()
                if can_check_signedin:
                    print("galaxy signedin " + str(signedin))
                    return signedin
                else:
                    print("galaxy signedin_check failed")
                    return False
            except Exception, e:
                print("galaxy signedin_check failed: " + str(e))
                return False

        def has(self, achievement_name):
            if not self.enabled or not self.galaxy or not self.renpygalaxy:
                return

            try:
                can_check_ach = self.renpygalaxy.CheckGetAchievement(
                    achievement_name)
                get_ach = self.renpygalaxy.GetAchievement(achievement_name)
                if can_check_ach:
                    print("galaxy ach:(" +
                          achievement_name + ") unlocked: " + str(get_ach))
                    return get_ach
                else:
                    print("can't check if galaxy has ach:(" +
                          achievement_name + ")")
                    return False
            except Exception, e:
                print("galaxy has() failed: " + str(e))
                return False

        def grant(self, achievement_name):
            if not self.enabled or not self.galaxy or not self.renpygalaxy:
                return

            if not self.signed_in():
                print("galaxy grant ach:(" +
                      achievement_name + ") failed. not signed in")
                return

            try:
                if not self.has(achievement_name):
                    ach_set = self.renpygalaxy.SetAchievement(achievement_name)
                    if ach_set:
                        print("galaxy ach:(" +
                              achievement_name + ") set")
                        ach_stored = self.renpygalaxy.StoreStatsAndAchievements()
                        if ach_stored:
                            print("galaxy ach:(" +
                                  achievement_name + ") stored")
                    else:
                        print("galaxy ach:(" +
                              achievement_name + ") set failed")
                else:
                    print("galaxy ach:(" +
                          achievement_name + ") already unlocked")
            except Exception, e:
                print("galaxy grant() failed: " + str(e))

        def sync(self):
            if not self.enabled or not self.galaxy or not self.renpygalaxy:
                return

            if not self.signed_in():
                print("galaxy sync ach failed. not signed in")
                return

            for ach_name in persistent._achievements:
                if not self.has(ach_name):
                    print("galaxy ach:(" +
                          ach_name + ") sync: granted")
                    self.grant(ach_name)
